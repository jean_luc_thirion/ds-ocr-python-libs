import numpy as np
from enum import Enum

class Strategie(Enum):
    CHANGER = 1
    GARDER = 2
    
def play(strategie, nb_tours):
    '''Simule une suite de tours du jeu.
    
    Cette fonction renvoie les résultats de plusieurs parties
    du jeu Monty Hall sous forme d'une liste de gains par le 
    joueur.
    
    Args:
        strategie (Strategie): La strategie du joueur
        nb_tours (int): Nombre de tours
        
    Returns:
        list: Liste des gains du joueurs à chaque partie
    '''
    bonnes_portes = np.random.randint(0, 3, size = nb_tours)
    premier_choix = np.random.randint(0, 3, size = nb_tours)
    
    return premier_choix == bonnes_portes if strategie == Strategie.GARDER else premier_choix != bonnes_portes


def nb_victoires(strategie): 
    '''Produit une lambda qui comptabilise le nombre de victoires.
    
    Cette lambda prend en argument un nombre de tours, simule le nombre de parties spécifié en utilisant
    la stratégie spécifiée en argument, et comptabilise le nombre de victoires
    
    Args:
        strategie (Strategie): La strategie du joueur
        
    Returns:
        list: une lambda qui prend en argument un nombre de tours à simuler et comptabilise le nombre de victoires
    '''
    return lambda tours : np.sum(play(strategie, tours))
   
        